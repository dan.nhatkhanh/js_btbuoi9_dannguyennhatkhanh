// var tdList = document.querySelectorAll(".td-scores");
// console.log("tdList: ", tdList);

// for (var index = 0; index < tdList.length; index++) {
//   var curentIndex = tdList[index];
//   console.log(curentIndex.innerText);
// }

// var trList = document.querySelectorAll("#tblBody tr");
// console.log("trList: ", trList);

// var trResult = trList[0];

// var tdListResult = trResult.querySelectorAll("td");
// console.log(tdListResult[2].innerText);

// // for (var index = 0; index < trList.length; index++) {
// //   var curentIndex = trList[index];
// //   console.log(curentIndex.innerText);
// // }

// arr = [1, 9, 8, 5];
// max = arr[0];

function getScoreFromTr(trTag) {
  var tdList = trTag.querySelectorAll("td");

  return tdList[3].innerText * 1;
}

function getNameFromTr(trTag) {
  var tdList = trTag.querySelectorAll("td");

  return tdList[2].innerText;
}

var trList = document.querySelectorAll("#tblBody tr");

var trMax = trList[0];

var trMin = trList[0];

for (var index = 0; index < trList.length; index++) {
  var currentTr = trList[index];

  // điểm của tr hiện tại
  var currentScore = getScoreFromTr(currentTr);

  // điểm của tr lớn nhất
  var currentScoreMax = getScoreFromTr(trMax);

  if (currentScore > currentScoreMax) {
    trMax = currentTr;
  }

  // điểm của tr nhỏ nhất
  var currentScoreMin = getScoreFromTr(trMin);

  if (currentScore < currentScoreMin) {
    trMin = currentTr;
  }
}

function svCaoDiemNhat() {
  document.querySelector("#svGioiNhat").innerHTML = `${getNameFromTr(
    trMax
  )} - ${getScoreFromTr(trMax)}`;
}

function svThapDiemNhat() {
  document.querySelector("#svYeuNhat").innerHTML = `${getNameFromTr(
    trMin
  )} - ${getScoreFromTr(trMin)}`;
}

var dsHon5 = "";

var arrSinhVienGioi = Array.from(trList).filter((trTag) => {
  return getScoreFromTr(trTag) >= 8;
});

function demSVGioi() {
  document.querySelector("#soSVGioi").innerHTML = arrSinhVienGioi.length;
}

var arrSinhVienCoDiemHon5 = Array.from(trList).filter((trTag) => {
  return getScoreFromTr(trTag) > 5;
});

var trHon5 = arrSinhVienCoDiemHon5[0];

for (var index = 0; index < arrSinhVienCoDiemHon5.length; index++) {
  var currentTr = arrSinhVienCoDiemHon5[index];

  trHon5 = currentTr;

  dsHon5 += getScoreFromTr(trHon5) + " - " + getNameFromTr(trHon5) + "<br>";
}

function danhSachSvDiemHon5() {
  document.querySelector("#dsDiemHon5").innerHTML = `<p>${dsHon5}</p>`;
}

var dsSapXep = "";

var scoreArr = [];
var trSapXep = trList[0];

for (var index = 0; index < trList.length; index++) {
  var currentTr = trList[index];

  trSapXep = currentTr;

  scoreArr.push(
    getScoreFromTr(trSapXep) + " - " + getNameFromTr(trSapXep) + " "
  );

  scoreArr.sort();
}

var trDsSapXep = scoreArr[0];

for (var index = 0; index < scoreArr.length; index++) {
  var currentTr = scoreArr[index];

  trDsSapXep = currentTr;

  dsSapXep += trDsSapXep + "<br>";
}

function sxTangDan() {
  document.getElementById(`dtbTang`).innerHTML = ` <p>${dsSapXep} <br></p>`;
}
